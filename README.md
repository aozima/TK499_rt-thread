# TK499

TK499 是 [好钜润半导体](http://www.hjrkj.com/) 公司的产品。

## 相关链接

* [挖坑网论坛版面](https://whycan.cn/f_42.html)
* [阿莫论坛版面](https://www.amobbs.com/forum-9982-1.html)

## 使用说明

1. 使用MDK5.2x编译得到 `rtthread.bin`。
2. 使用官方的boot，把 `rtthread.bin` 复制到U盘中。
3. LED(PD8)会闪
4. uart波特率460800，使用终端软件连接上去，可以执行shell命令。

## 已支持功能
* [x] 基本系统
* [x] `uart1`驱动
* [x] GPIO驱动（支持中断功能）

## TODO
* [ ] 波特率不能使用最常用的115200。
* [ ] 需要支持更多的`uart`驱动
* [ ] 需要支持`spi`驱动
* [ ] 需要支持`sdio`驱动
* [ ] 尝试支持lcd驱动
