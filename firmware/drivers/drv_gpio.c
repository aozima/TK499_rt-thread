/*
 * Copyright (c) 2006-2019, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author            Notes
 * 2019-11-16     aozima            first version
 */

#include <rtthread.h>
#include <rthw.h>
#include <board.h>

#ifdef RT_USING_PIN

#include <rtdevice.h>
#include "drv_gpio.h"

static const struct pin_index pins[] = 
{
    /* GPIOA */
    __TK499_PIN( 0 + 0 ,  A, 0 ),
    __TK499_PIN( 0 + 1 ,  A, 1 ),
    __TK499_PIN( 0 + 2 ,  A, 2 ),
    __TK499_PIN( 0 + 3 ,  A, 3 ),
    __TK499_PIN( 0 + 4 ,  A, 4 ),
    __TK499_PIN( 0 + 5 ,  A, 5 ),
    __TK499_PIN( 0 + 6 ,  A, 6 ),
    __TK499_PIN( 0 + 7 ,  A, 7 ),
    __TK499_PIN( 0 + 8 ,  A, 8 ),
    __TK499_PIN( 0 + 9 ,  A, 9 ),
    __TK499_PIN( 0 + 10,  A, 10),
    __TK499_PIN( 0 + 11,  A, 11),
    __TK499_PIN( 0 + 12,  A, 12),
    __TK499_PIN( 0 + 13,  A, 13),
    __TK499_PIN( 0 + 14,  A, 14),
    __TK499_PIN( 0 + 15,  A, 15),

    /* GPIOB */
    __TK499_PIN(16 + 0 ,  B, 0 ),
    __TK499_PIN(16 + 1 ,  B, 1 ),
    __TK499_PIN(16 + 2 ,  B, 2 ),
    __TK499_PIN(16 + 3 ,  B, 3 ),
    __TK499_PIN(16 + 4 ,  B, 4 ),
    __TK499_PIN(16 + 5 ,  B, 5 ),
    __TK499_PIN(16 + 6 ,  B, 6 ),
    __TK499_PIN(16 + 7 ,  B, 7 ),
    __TK499_PIN(16 + 8 ,  B, 8 ),
    __TK499_PIN(16 + 9 ,  B, 9 ),
    __TK499_PIN(16 + 10,  B, 10),
    __TK499_PIN(16 + 11,  B, 11),
    __TK499_PIN(16 + 12,  B, 12),
    __TK499_PIN(16 + 13,  B, 13),
    __TK499_PIN(16 + 14,  B, 14),
    __TK499_PIN(16 + 15,  B, 15),

    /* GPIOC */
    __TK499_PIN(32 + 0 ,  C, 0 ),
    __TK499_PIN(32 + 1 ,  C, 1 ),
    __TK499_PIN(32 + 2 ,  C, 2 ),
    __TK499_PIN(32 + 3 ,  C, 3 ),
    __TK499_PIN(32 + 4 ,  C, 4 ),
    __TK499_PIN(32 + 5 ,  C, 5 ),
    __TK499_PIN(32 + 6 ,  C, 6 ),
    __TK499_PIN(32 + 7 ,  C, 7 ),
    __TK499_PIN(32 + 8 ,  C, 8 ),
    __TK499_PIN(32 + 9 ,  C, 9 ),
    __TK499_PIN(32 + 10,  C, 10),
    __TK499_PIN(32 + 11,  C, 11),
    __TK499_PIN(32 + 12,  C, 12),
    __TK499_PIN(32 + 13,  C, 13),
    __TK499_PIN(32 + 14,  C, 14),
    __TK499_PIN(32 + 15,  C, 15),

    /* GPIOD */
    __TK499_PIN(48 + 0 ,  D, 0 ),
    __TK499_PIN(48 + 1 ,  D, 1 ),
    __TK499_PIN(48 + 2 ,  D, 2 ),
    __TK499_PIN(48 + 3 ,  D, 3 ),
    __TK499_PIN(48 + 4 ,  D, 4 ),
    __TK499_PIN(48 + 5 ,  D, 5 ),
    __TK499_PIN(48 + 6 ,  D, 6 ),
    __TK499_PIN(48 + 7 ,  D, 7 ),
    __TK499_PIN(48 + 8 ,  D, 8 ),
    __TK499_PIN(48 + 9 ,  D, 9 ),
    __TK499_PIN(48 + 10,  D, 10),
    __TK499_PIN(48 + 11,  D, 11),
    __TK499_PIN(48 + 12,  D, 12),
    __TK499_PIN(48 + 13,  D, 13),
    __TK499_PIN(48 + 14,  D, 14),
    __TK499_PIN(48 + 15,  D, 15),

    /* GPIOE */
    __TK499_PIN(64 + 0 ,  E, 0 ),
    __TK499_PIN(64 + 1 ,  E, 1 ),
    __TK499_PIN(64 + 2 ,  E, 2 ),
    __TK499_PIN(64 + 3 ,  E, 3 ),
    __TK499_PIN(64 + 4 ,  E, 4 ),
    __TK499_PIN(64 + 5 ,  E, 5 ),
    __TK499_PIN(64 + 6 ,  E, 6 ),
    __TK499_PIN(64 + 7 ,  E, 7 ),
    __TK499_PIN(64 + 8 ,  E, 8 ),
    __TK499_PIN(64 + 9 ,  E, 9 ),
    __TK499_PIN(64 + 10,  E, 10),
    __TK499_PIN(64 + 11,  E, 11),
    __TK499_PIN(64 + 12,  E, 12),
    __TK499_PIN(64 + 13,  E, 13),
    __TK499_PIN(64 + 14,  E, 14),
    __TK499_PIN(64 + 15,  E, 15),
};

static const struct pin_irq_map pin_irq_map[] =
{
    {GPIO_Pin_0, EXTI0_IRQn},
    {GPIO_Pin_1, EXTI1_IRQn},
    {GPIO_Pin_2, EXTI2_IRQn},
    {GPIO_Pin_3, EXTI3_IRQn},
    {GPIO_Pin_4, EXTI4_IRQn},
    {GPIO_Pin_5, EXTI9_5_IRQn},
    {GPIO_Pin_6, EXTI9_5_IRQn},
    {GPIO_Pin_7, EXTI9_5_IRQn},
    {GPIO_Pin_8, EXTI9_5_IRQn},
    {GPIO_Pin_9, EXTI9_5_IRQn},
    {GPIO_Pin_10, EXTI15_10_IRQn},
    {GPIO_Pin_11, EXTI15_10_IRQn},
    {GPIO_Pin_12, EXTI15_10_IRQn},
    {GPIO_Pin_13, EXTI15_10_IRQn},
    {GPIO_Pin_14, EXTI15_10_IRQn},
    {GPIO_Pin_15, EXTI15_10_IRQn},
};

static struct rt_pin_irq_hdr pin_irq_hdr_tab[] =
{
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
};
static uint32_t pin_irq_enable_mask=0;

#ifndef ITEM_NUM
#define ITEM_NUM(items) sizeof(items) / sizeof(items[0])
#endif /* ITEM_NUM */
static const struct pin_index *get_pin(uint8_t pin)
{
    const struct pin_index *index;

    if (pin < ITEM_NUM(pins))
    {
        index = &pins[pin];
        if (index->index == -1)
            index = RT_NULL;
    }
    else
    {
        index = RT_NULL;
    }

    return index;
};

rt_inline rt_int32_t bit2bitno(rt_uint32_t bit)
{
    int i;
    for (i = 0; i < 32; i++)
    {
        if ((0x01 << i) == bit)
        {
            return i;
        }
    }
    return -1;
}

rt_inline const struct pin_irq_map *get_pin_irq_map(uint32_t pinbit)
{
    rt_int32_t mapindex = bit2bitno(pinbit);
    if (mapindex < 0 || mapindex >= ITEM_NUM(pin_irq_map))
    {
        return RT_NULL;
    }
    return &pin_irq_map[mapindex];
};

static void tk499_pin_mode(rt_device_t dev, rt_base_t pin, rt_base_t mode)
{
    const struct pin_index *index;
    GPIO_InitTypeDef GPIO_InitStructure;

    index = get_pin(pin);
    if (index == RT_NULL)
    {
        return;
    }

    GPIO_InitStructure.GPIO_Pin = index->pin;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;

    if (mode == PIN_MODE_OUTPUT)
    {
        /* output setting */
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    }
    else if (mode == PIN_MODE_INPUT)
    {
        /* input setting: not pull. */
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    }
    else if (mode == PIN_MODE_INPUT_PULLUP)
    {
        /* input setting: pull up. */
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    }
    else if (mode == PIN_MODE_INPUT_PULLDOWN)
    {
        /* input setting: pull down. */
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
    }
    else if (mode == PIN_MODE_OUTPUT_OD)
    {
        /* output setting: od. */
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
    }

    GPIO_Init(index->gpio, &GPIO_InitStructure);
}

static void tk499_pin_write(rt_device_t dev, rt_base_t pin, rt_base_t value)
{
    const struct pin_index *index;

    index = get_pin(pin);
    if (index == RT_NULL)
    {
        return;
    }

    GPIO_WriteBit(index->gpio, index->pin, (BitAction)value);
}

static int tk499_pin_read(rt_device_t dev, rt_base_t pin)
{
    int value;
    const struct pin_index *index;

    value = PIN_LOW;

    index = get_pin(pin);
    if (index == RT_NULL)
    {
        return value;
    }

    value = GPIO_ReadInputDataBit(index->gpio, index->pin);

    return value;
}

static rt_err_t tk499_pin_attach_irq(struct rt_device *device, rt_int32_t pin,
                                     rt_uint32_t mode, void (*hdr)(void *args), void *args)
{
    const struct pin_index *index;
    rt_base_t level;
    rt_int32_t irqindex = -1;

    index = get_pin(pin);
    if (index == RT_NULL)
    {
        return RT_ENOSYS;
    }

    irqindex = bit2bitno(index->pin);
    if (irqindex < 0 || irqindex >= ITEM_NUM(pin_irq_map))
    {
        return RT_ENOSYS;
    }

    level = rt_hw_interrupt_disable();
    if (pin_irq_hdr_tab[irqindex].pin == pin &&
        pin_irq_hdr_tab[irqindex].hdr == hdr &&
        pin_irq_hdr_tab[irqindex].mode == mode &&
        pin_irq_hdr_tab[irqindex].args == args)
    {
        rt_hw_interrupt_enable(level);
        return RT_EOK;
    }

    if (pin_irq_hdr_tab[irqindex].pin != -1)
    {
        rt_hw_interrupt_enable(level);
        return RT_EBUSY;
    }

    pin_irq_hdr_tab[irqindex].pin = pin;
    pin_irq_hdr_tab[irqindex].hdr = hdr;
    pin_irq_hdr_tab[irqindex].mode = mode;
    pin_irq_hdr_tab[irqindex].args = args;
    rt_hw_interrupt_enable(level);

    return RT_EOK;
}

static rt_err_t tk499_pin_dettach_irq(struct rt_device *device, rt_int32_t pin)
{
    const struct pin_index *index;
    rt_base_t level;
    rt_int32_t irqindex = -1;

    index = get_pin(pin);
    if (index == RT_NULL)
    {
        return RT_ENOSYS;
    }

    irqindex = bit2bitno(index->pin);
    if (irqindex < 0 || irqindex >= ITEM_NUM(pin_irq_map))
    {
        return RT_ENOSYS;
    }

    level = rt_hw_interrupt_disable();
    if (pin_irq_hdr_tab[irqindex].pin == -1)
    {
        rt_hw_interrupt_enable(level);
        return RT_EOK;
    }

    pin_irq_hdr_tab[irqindex].pin = -1;
    pin_irq_hdr_tab[irqindex].hdr = RT_NULL;
    pin_irq_hdr_tab[irqindex].mode = 0;
    pin_irq_hdr_tab[irqindex].args = RT_NULL;
    rt_hw_interrupt_enable(level);

    return RT_EOK;
}

/*
* @brief  config exit interrupt.
* @param GPIOx: where x can be (0-4).
* @param BITx: where x can be (0-4).
* @param TRIM: 0: disable, 1:FALLING, 2:RISING, 3:RISING_FALLING.
* @retval : None
*/
static void Ex_NVIC_Config(uint32_t GPIOx, uint32_t BITx, uint32_t TRIM)
{
    uint32_t EXTOFFSET = (BITx % 4) * 4;

    // rt_kprintf("%s L%d, GPIOx=%d BITx=%d TRIM=%d EXTOFFSET=%d\n", __FUNCTION__, __LINE__, GPIOx, BITx, TRIM, EXTOFFSET);

    RCC->APB2ENR |= 1 << 14;                            // enable SYSCFG clock.
    SYSCFG->EXTICR[BITx / 4] &= ~(0x000F << EXTOFFSET); // clean old setting.
    SYSCFG->EXTICR[BITx / 4] |= GPIOx << EXTOFFSET;     // reg to IO.

    EXTI->IMR |= 1 << BITx; // enable EXTI_Line.
    if (TRIM & 0x01)
    {
        EXTI->FTSR |= 1 << BITx;
    }

    if (TRIM & 0x02)
    {
        EXTI->RTSR |= 1 << BITx;
    }
}

static rt_err_t tk499_pin_irq_enable(struct rt_device *device, rt_base_t pin,
                                     rt_uint32_t enabled)
{
    const struct pin_index *index;
    const struct pin_irq_map *irqmap;
    rt_base_t level;
    rt_int32_t irqindex = -1;
    GPIO_InitTypeDef GPIO_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    index = get_pin(pin);
    if (index == RT_NULL)
    {
        return RT_ENOSYS;
    }

    if (enabled == PIN_IRQ_ENABLE)
    {
        uint32_t TRIM = 0;

        irqindex = bit2bitno(index->pin);
        if (irqindex < 0 || irqindex >= ITEM_NUM(pin_irq_map))
        {
            return RT_ENOSYS;
        }

        level = rt_hw_interrupt_disable();

        if (pin_irq_hdr_tab[irqindex].pin == -1)
        {
            rt_hw_interrupt_enable(level);
            return RT_ENOSYS;
        }

        irqmap = &pin_irq_map[irqindex];

        /* Configure GPIO_InitStructure */
        GPIO_InitStructure.GPIO_Pin = index->pin;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        switch (pin_irq_hdr_tab[irqindex].mode)
        {
        case PIN_IRQ_MODE_RISING:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
            TRIM = 2;
            break;

        case PIN_IRQ_MODE_FALLING:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
            TRIM = 1;
            break;

        case PIN_IRQ_MODE_RISING_FALLING:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
            TRIM = 3;
            break;

        default:
            return RT_EIO;
        }
        GPIO_Init(index->gpio, &GPIO_InitStructure);

        Ex_NVIC_Config(index->index / 16, bit2bitno(index->pin), TRIM);

        pin_irq_enable_mask |= irqmap->pinbit;

        NVIC_InitStructure.NVIC_IRQChannel = irqmap->irqno;
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
        NVIC_Init(&NVIC_InitStructure);

        rt_hw_interrupt_enable(level);
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        irqmap = get_pin_irq_map(index->pin);
        if (irqmap == RT_NULL)
        {
            return RT_ENOSYS;
        }

        level = rt_hw_interrupt_disable();

        NVIC_InitStructure.NVIC_IRQChannel = irqmap->irqno;
        NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;

        pin_irq_enable_mask &= ~irqmap->pinbit;
        if ((irqmap->pinbit >= GPIO_Pin_5) && (irqmap->pinbit <= GPIO_Pin_9))
        {
            if (!(pin_irq_enable_mask & (GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9)))
            {
                NVIC_Init(&NVIC_InitStructure);
            }
        }
        else if ((irqmap->pinbit >= GPIO_Pin_10) && (irqmap->pinbit <= GPIO_Pin_15))
        {
            if (!(pin_irq_enable_mask & (GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15)))
            {
                NVIC_Init(&NVIC_InitStructure);
            }
        }
        else
        {
            NVIC_Init(&NVIC_InitStructure);
        }

        rt_hw_interrupt_enable(level);
    }
    else
    {
        return -RT_ENOSYS;
    }

    return RT_EOK;
}

static void _GPIO_EXTI_IRQHandler(uint32_t EXTI_Line)
{
    // rt_kprintf("%s L%d, EXTI_Line=%p\n", __FUNCTION__, __LINE__, EXTI_Line);
    if (EXTI_GetITStatus(EXTI_Line) != RESET)
    {
        int irqno;

        EXTI_ClearITPendingBit(EXTI_Line);

        irqno = bit2bitno(EXTI_Line);
        if (pin_irq_hdr_tab[irqno].hdr)
        {
            pin_irq_hdr_tab[irqno].hdr(pin_irq_hdr_tab[irqno].args);
        }
    }
}

void EXTI0_IRQHandler(void)
{
    rt_interrupt_enter();
    _GPIO_EXTI_IRQHandler(EXTI_Line0);
    rt_interrupt_leave();
}

void EXTI1_IRQHandler(void)
{
    rt_interrupt_enter();
    _GPIO_EXTI_IRQHandler(EXTI_Line1);
    rt_interrupt_leave();
}

void EXTI2_IRQHandler(void)
{
    rt_interrupt_enter();
    _GPIO_EXTI_IRQHandler(EXTI_Line2);
    rt_interrupt_leave();
}

void EXTI3_IRQHandler(void)
{
    rt_interrupt_enter();
    _GPIO_EXTI_IRQHandler(EXTI_Line3);
    rt_interrupt_leave();
}

void EXTI4_IRQHandler(void)
{
    rt_interrupt_enter();
    _GPIO_EXTI_IRQHandler(EXTI_Line4);
    rt_interrupt_leave();
}

void EXTI9_5_IRQHandler(void)
{
    rt_interrupt_enter();
    _GPIO_EXTI_IRQHandler(EXTI_Line5);
    _GPIO_EXTI_IRQHandler(EXTI_Line6);
    _GPIO_EXTI_IRQHandler(EXTI_Line7);
    _GPIO_EXTI_IRQHandler(EXTI_Line8);
    _GPIO_EXTI_IRQHandler(EXTI_Line9);
    rt_interrupt_leave();
}

void EXTI15_10_IRQHandler(void)
{
    rt_interrupt_enter();
    _GPIO_EXTI_IRQHandler(EXTI_Line10);
    _GPIO_EXTI_IRQHandler(EXTI_Line11);
    _GPIO_EXTI_IRQHandler(EXTI_Line12);
    _GPIO_EXTI_IRQHandler(EXTI_Line13);
    _GPIO_EXTI_IRQHandler(EXTI_Line14);
    _GPIO_EXTI_IRQHandler(EXTI_Line15);
    rt_interrupt_leave();
}

static const struct rt_pin_ops _tk499_pin_ops =
{
    tk499_pin_mode,
    tk499_pin_write,
    tk499_pin_read,
    tk499_pin_attach_irq,
    tk499_pin_dettach_irq,
    tk499_pin_irq_enable,
};

int rt_hw_pin_init(void)
{
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOD, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOE, ENABLE);

    return rt_device_pin_register("pin", &_tk499_pin_ops, RT_NULL);
}

#endif /* RT_USING_PIN */
