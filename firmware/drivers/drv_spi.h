/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-5      SummerGift   first version
 */

#ifndef __DRV_SPI_H_
#define __DRV_SPI_H_

#include <rtthread.h>
#include "rtdevice.h"
#include <rthw.h>
#include "HAL_device.h"

rt_err_t rt_hw_spi_device_attach(const char *bus_name, const char *device_name, GPIO_TypeDef* cs_gpiox, uint16_t cs_gpio_pin);

struct tk499_hw_spi_cs
{
    GPIO_TypeDef* GPIOx;
    uint16_t GPIO_Pin;
};

struct tk499_spi_config
{
    SPI_TypeDef *Instance;
    char *bus_name;
    GPIO_TypeDef* GPIOx;
    uint16_t CLK_GPIO_Pin;
    uint16_t MOSI_GPIO_Pin;
    uint16_t MISO_GPIO_Pin;
    uint32_t RCC_AHBPeriph;
    uint32_t RCC_APB2Periph;
};

struct tk499_spi_device
{
    rt_uint32_t pin;
    char *bus_name;
    char *device_name;
};

#define SPI_USING_RX_DMA_FLAG   (1<<0)
#define SPI_USING_TX_DMA_FLAG   (1<<1)

typedef struct __SPI_HandleTypeDef
{
    SPI_TypeDef *Instance;
    SPI_InitTypeDef Init;
}SPI_HandleTypeDef;

/* stm32 spi dirver class */
struct tk499_spi
{
    SPI_HandleTypeDef handle;
    struct tk499_spi_config *config;
    struct rt_spi_configuration *cfg;
    
    rt_uint8_t spi_dma_flag;
    struct rt_spi_bus spi_bus;
};

#endif /*__DRV_SPI_H_ */
