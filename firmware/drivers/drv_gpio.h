/*
 * Copyright (c) 2006-2019, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author            Notes
 * 2019-11-16     aozima            first version
 */

#ifndef __DRV_GPIO_H__
#define __DRV_GPIO_H__

#include <rtthread.h>
#include <board.h>

#include <rtdevice.h>
#include "HAL_conf.h"

/* GPIOA_BASE */
#define __TK499_PORT(port)      GPIO##port##_BASE

/* GPIOA ==> 0, GPIOB ==> 1. */
#define __TK499_PORT_NUM(port)  (((rt_base_t)__TK499_PORT(port) - (rt_base_t)GPIOA_BASE) / (0x0400UL))

/* GET_PIN(A, 1 ) ==> 1, GET_PIN(B, 0 ) ==> 16. */
#define GET_PIN(PORTx,PIN)      (rt_base_t)(16 * __TK499_PORT_NUM(PORTx) + PIN)

/* {16 + 8, GPIOB, GPIO_Pin_8} */
#define __TK499_PIN(index, gpio, gpio_index)                                \
    {                                                                       \
        index, GPIO##gpio, GPIO_Pin_##gpio_index                            \
    }

#define __TK499_PIN_RESERVE                                                 \
    {                                                                       \
        -1, 0, 0                                                            \
    }

/* TK499 GPIO driver */
struct pin_index
{
    int index;
    GPIO_TypeDef *gpio;
    uint32_t pin;
};

struct pin_irq_map
{
    rt_uint16_t pinbit;
    IRQn_Type irqno;
};

int rt_hw_pin_init(void);

#endif /* __DRV_GPIO_H__ */
