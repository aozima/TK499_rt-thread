#include <rtthread.h>

#include "board.h" 
#include "HAL_conf.h"

#include "drv_gpio.h"
#include "drv_spi.h"
#include "HAL_device.h"

int main(int argc, char **argv)
{
    int led_pin;

    led_pin = GET_PIN(D, 8);
    rt_pin_mode(led_pin, PIN_MODE_OUTPUT);

    while (1)
    {
        rt_pin_write(led_pin, 1);
        rt_thread_delay(RT_TICK_PER_SECOND);

        rt_pin_write(led_pin, 0);
        rt_thread_delay(RT_TICK_PER_SECOND);
    }

    return 0;
}

// int spi_init()
// {
//     GPIO_TypeDef *cs_gpiox;
//     uint16_t cs_pin;
//     struct rt_spi_device *spi_device;

//     cs_gpiox = (GPIO_TypeDef *)((rt_base_t)GPIOA + (rt_base_t)(RW007_CS_PIN / 16) * 0x0400UL);
//     cs_pin = (uint16_t)(1 << RW007_CS_PIN % 16);

//     rt_hw_spi_device_attach(RW007_SPI_BUS_NAME, RW007_DEV_NAME, cs_gpiox, cs_pin);

//     spi_device = (struct rt_spi_device *)rt_device_find(RW007_DEV_NAME);

//     if (spi_device == RT_NULL)
//     {
//         rt_kprintf("spi device %s not found!\r", RW007_SPI_BUS_NAME);
//         return -RT_ENOSYS;
//     }

//     /* config spi */
//     {
//         struct rt_spi_configuration cfg;
//         cfg.data_width = 8;
//         cfg.mode = RT_SPI_MODE_MASK; /* SPI Compatible: Mode 0. */
//         cfg.max_hz = 30 * 1000000;             /* 15M 007 max 30M */
//         rt_spi_configure(spi_device, &cfg);
//     }

//     return 0;
// }
// INIT_DEVICE_EXPORT(spi_init);

// void spi_send_data()
// {
// 	char send[10] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0xa0};
// 	struct rt_spi_device *rt_spi_device = (struct rt_spi_device *)rt_device_find(RW007_DEV_NAME);
	
// 	rt_spi_transfer(rt_spi_device, send, RT_NULL, 10);
	
// }
// MSH_CMD_EXPORT(spi_send_data, spi_send_data);

