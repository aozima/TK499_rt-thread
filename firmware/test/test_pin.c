#include <rthw.h>
#include <rtthread.h>
#include <rtdevice.h>

#include "board.h" 

/*
PA8 : RIGHT
PA1 : UP.    2nd boot.
PA13: LEFT.  1st boot.
PA0 : CENTER
PB13: DOWN
*/

static int pin_read_test(int argc, char **argv)
{
    int pin_right = GET_PIN(A, 8);
    int pin_up = GET_PIN(A, 1);
    int pin_left = GET_PIN(A, 13);
    int pin_center = GET_PIN(A, 0);
    int pin_down = GET_PIN(B, 13);

    rt_tick_t tick_start, tick_end;

    rt_pin_mode(pin_right, PIN_MODE_INPUT_PULLDOWN);
    rt_pin_mode(pin_up, PIN_MODE_INPUT_PULLDOWN);
    rt_pin_mode(pin_left, PIN_MODE_INPUT_PULLDOWN);
    rt_pin_mode(pin_center, PIN_MODE_INPUT_PULLDOWN);
    rt_pin_mode(pin_down, PIN_MODE_INPUT_PULLDOWN);

    tick_start = rt_tick_get();
    while (1)
    {
        tick_end = rt_tick_get();
        if ((tick_end - tick_start) > (RT_TICK_PER_SECOND * 10))
        {
            rt_kprintf("timeout, exit!\n");
            break;
        }

        if(rt_pin_read(pin_right))
        {
            rt_kprintf("pin_right!\n");
        }

        if(rt_pin_read(pin_up))
        {
            rt_kprintf("pin_up!\n");
        }

        if(rt_pin_read(pin_left))
        {
            rt_kprintf("pin_left!\n");
        }

        if(rt_pin_read(pin_center))
        {
            rt_kprintf("pin_center!\n");
        }

        if(rt_pin_read(pin_down))
        {
            rt_kprintf("pin_down!\n");
        }

        rt_thread_delay(RT_TICK_PER_SECOND / 10);
    }

    return 0;
}
MSH_CMD_EXPORT(pin_read_test, pin_read_test);

static void pin_isr(void *args)
{
    int pin = (int)args;
    rt_kprintf("%s L%d, pin=%d\n", __FUNCTION__, __LINE__, pin);
}

static int pin_irq_test(int argc, char **argv)
{
    int pin_right = GET_PIN(A, 8);
    int pin_up = GET_PIN(A, 1);
    int pin_left = GET_PIN(A, 13);
    int pin_center = GET_PIN(A, 0);
    int pin_down = GET_PIN(B, 13);

    rt_err_t ret;
    rt_tick_t tick_start, tick_end;

    rt_pin_mode(pin_right, PIN_MODE_INPUT_PULLDOWN);
    rt_pin_mode(pin_up, PIN_MODE_INPUT_PULLDOWN);
    rt_pin_mode(pin_left, PIN_MODE_INPUT_PULLDOWN);
    rt_pin_mode(pin_center, PIN_MODE_INPUT_PULLDOWN);
    rt_pin_mode(pin_down, PIN_MODE_INPUT_PULLDOWN);

    ret = rt_pin_attach_irq(pin_right, PIN_IRQ_MODE_RISING,
                            pin_isr, (void *)pin_right);
    rt_kprintf("%s L%d, pin_right ret=%d\n", __FUNCTION__, __LINE__, ret);

    ret = rt_pin_attach_irq(pin_up, PIN_IRQ_MODE_RISING,
                            pin_isr, (void *)pin_up);
    rt_kprintf("%s L%d, pin_up ret=%d\n", __FUNCTION__, __LINE__, ret);

    ret = rt_pin_attach_irq(pin_left, PIN_IRQ_MODE_RISING,
                            pin_isr, (void *)pin_left);
    rt_kprintf("%s L%d, pin_left ret=%d\n", __FUNCTION__, __LINE__, ret);

    ret = rt_pin_attach_irq(pin_center, PIN_IRQ_MODE_RISING,
                            pin_isr, (void *)pin_center);
    rt_kprintf("%s L%d, pin_center ret=%d\n", __FUNCTION__, __LINE__, ret);

    ret = rt_pin_attach_irq(pin_down, PIN_IRQ_MODE_RISING,
                            pin_isr, (void *)pin_down);
    rt_kprintf("%s L%d, pin_down ret=%d\n", __FUNCTION__, __LINE__, ret);

    ret = rt_pin_irq_enable(pin_right, PIN_IRQ_ENABLE);
    rt_kprintf("%s L%d, pin_right ret=%d\n", __FUNCTION__, __LINE__, ret);
    ret = rt_pin_irq_enable(pin_up, PIN_IRQ_ENABLE);
    rt_kprintf("%s L%d, pin_up ret=%d\n", __FUNCTION__, __LINE__, ret);
    ret = rt_pin_irq_enable(pin_left, PIN_IRQ_ENABLE);
    rt_kprintf("%s L%d, pin_left ret=%d\n", __FUNCTION__, __LINE__, ret);
    ret = rt_pin_irq_enable(pin_center, PIN_IRQ_ENABLE);
    rt_kprintf("%s L%d, pin_center ret=%d\n", __FUNCTION__, __LINE__, ret);
    ret = rt_pin_irq_enable(pin_down, PIN_IRQ_ENABLE);
    rt_kprintf("%s L%d, pin_down ret=%d\n", __FUNCTION__, __LINE__, ret);

    tick_start = rt_tick_get();
    while (1)
    {
        tick_end = rt_tick_get();
        if ((tick_end - tick_start) > (RT_TICK_PER_SECOND * 15))
        {
            rt_kprintf("timeout, exit!\n");
            break;
        }
    }

    ret = rt_pin_irq_enable(pin_right, PIN_IRQ_DISABLE);
    rt_kprintf("%s L%d, pin_right ret=%d\n", __FUNCTION__, __LINE__, ret);
    ret = rt_pin_irq_enable(pin_up, PIN_IRQ_DISABLE);
    rt_kprintf("%s L%d, pin_up ret=%d\n", __FUNCTION__, __LINE__, ret);
    ret = rt_pin_irq_enable(pin_left, PIN_IRQ_DISABLE);
    rt_kprintf("%s L%d, pin_left ret=%d\n", __FUNCTION__, __LINE__, ret);
    ret = rt_pin_irq_enable(pin_center, PIN_IRQ_DISABLE);
    rt_kprintf("%s L%d, pin_center ret=%d\n", __FUNCTION__, __LINE__, ret);
    ret = rt_pin_irq_enable(pin_down, PIN_IRQ_DISABLE);
    rt_kprintf("%s L%d, pin_down ret=%d\n", __FUNCTION__, __LINE__, ret);

    ret = rt_pin_detach_irq(pin_right);
    rt_kprintf("%s L%d, pin_right ret=%d\n", __FUNCTION__, __LINE__, ret);
    ret = rt_pin_detach_irq(pin_up);
    rt_kprintf("%s L%d, pin_up ret=%d\n", __FUNCTION__, __LINE__, ret);
    ret = rt_pin_detach_irq(pin_left);
    rt_kprintf("%s L%d, pin_left ret=%d\n", __FUNCTION__, __LINE__, ret);
    ret = rt_pin_detach_irq(pin_center);
    rt_kprintf("%s L%d, pin_center ret=%d\n", __FUNCTION__, __LINE__, ret);
    ret = rt_pin_detach_irq(pin_down);
    rt_kprintf("%s L%d, pin_down ret=%d\n", __FUNCTION__, __LINE__, ret);

    return 0;
}
MSH_CMD_EXPORT(pin_irq_test, pin_irq_test);
